#config
. .\config.ps1

# Define input properties and default values.
properties                                                               {
	 
    # jMeter
    $ParentPath                    = Split-Path (get-location).Path -Parent
    $javaLocation                  = (Get-ChildItem Env:JAVA_HOME).Value + "\bin"
    $jMeterLocation                = "$ParentPath\apache-jmeter-2.13\apache-jmeter-2.13\bin\jmeter"
    $jMeterTestPlanFolder          = Split-Path $jMeterTestPlanFile
    $jMeterCMDRunner               = "$ParentPath\apache-jmeter-2.13\apache-jmeter-2.13\lib\ext\CMDRunner.jar"    
    $jMeterTestResultOut           = "$ParentPath\JMeterOutput"
    $jMeterReportNameDatePrefix    = Get-Date
    $jMeterJTLReportFileName       = "spline.jtl"
    $jMeterJTLReportLocation       = "$ParentPath\JMeterOutput\$jMeterJTLReportFileName"

}


# Must contain a default task, it's run when no task is specified                                                                         
task default -depends LoadTest 


task LoadTest `
-depends RunLoadTest, MakeLoadTestCSV, ReadCsv `
-description "Runs load testing and creates output report"
 

task  RunLoadTest `
-description "Loads tests an existing website (see: http://jmeter.apache.org/usermanual/get-started.html )"           {

    Set-Location $javaLocation

    # delete the output file here that is made regardless of the -l switch where it is persisted for reports
    If (Test-Path "$jMeterJTLReportLocation"){
        Remove-Item "$jMeterJTLReportLocation"
    }

    $params = HashToJmeterParams($jMeterParams)
    $jmeterCommand = "$jMeterLocation -n $params -t ""$jMeterTestPlanFile"" -l ""$jMeterJTLReportLocation"""
    
    cmd.exe /c $jmeterCommand | ForEach-Object {
        $line = $_
        # Parse the listening port
        if ($line -match "^Waiting for possible shutdown message on port ([0-9]+)") {
           Write-host "Running Load Test: $jmeterCommand..."
            $jMeterListenerPort = $matches[1];
            return
        }
    }

    Set-Location $PSScriptRoot     
}


task  MakeLoadTestGraph `
-description "Makes a graph PNG file from the output of a report (see: http://jmeter-plugins.org/wiki/JMeterPluginsCMD/?utm_source=jpgc&utm_medium=link&utm_campaign=JMeterPluginsCMD )" {    
    Set-Location $javaLocation

    $jMeterReportNameDatePrefix = SimpleFileNameFriendlyDate($jMeterReportNameDatePrefix)
 
    & ./java.exe -jar $jMeterCMDRunner `
        --tool Reporter `
        --generate-png "$jMeterTestResultOut\$jMeterReportNameDatePrefix-graph.png" `
        --input-jtl $jMeterJTLReportLocation `
        --plugin-type ResponseTimesOverTime `
        --width 800 `
        --height 600

    Set-Location $PSScriptRoot
}


task  MakeLoadTestCSV `
-description "Makes a CSV file from the output of a report (see: http://jmeter-plugins.org/wiki/JMeterPluginsCMD/?utm_source=jpgc&utm_medium=link&utm_campaign=JMeterPluginsCMD )" {
    Set-Location $javaLocation

    $jMeterReportNameDatePrefix = SimpleFileNameFriendlyDate($jMeterReportNameDatePrefix)
 
    & ./java.exe -jar $jMeterCMDRunner `
        --tool Reporter `
        --generate-csv "$jMeterTestResultOut\$jMeterReportNameDatePrefix-details.csv" `
        --input-jtl $jMeterJTLReportLocation `
        --plugin-type AggregateReport 

    Set-Location $PSScriptRoot
}


task ReadCsv `
-description "Reads csv file" {
    
    $jMeterReportNameDatePrefix = SimpleFileNameFriendlyDate($jMeterReportNameDatePrefix)
    $csv = "$jMeterTestResultOut\$jMeterReportNameDatePrefix-details.csv"

    $stats = Import-Csv $csv | foreach {
      New-Object PSObject -prop @{
            Average = [int]$_.average;
            Median = [int]$_.aggregate_report_median;
            NinetyPercentLine = [int]$_."aggregate_report_90%_line";
            Min = [int]$_.aggregate_report_min;
            Max = [int]$_.aggregate_report_max;
            ErrorPercentage = [decimal]($_."aggregate_report_error%".SubString(0,$_."aggregate_report_error%".Length - 1))
        } 
    } | Select-Object -first 1

    $stats 

    AssertExpectation $MaxErrorPercentage "error percentage" $stats.ErrorPercentage
    AssertExpectation $AverageResponseTime "average response time" $stats.Average
}


task  StartJMeterUI `
-description "Launches jMeterUI for making tests"           {
    
    Set-Location $javaLocation
    & $jMeterLocation
    Set-Location $PSScriptRoot
}

function AssertExpectation($min,$desc, $actual) {
    if($actual -gt $min) {

        $msg = "TEST FAILED: wanted less then " + $min + " " + $desc +  " but got " + $actual 
        Write-host "-----------------------------------------------------------"
        Write-host $msg
        Write-host "-----------------------------------------------------------"
        throw $msg
    }
}

function HashToJmeterParams($hash) {
    $output = ""
    foreach ($h in $hash.GetEnumerator()) {
        $output+= "-J{0}=""{1}"" " -f $h.Name, $h.Value
    }
    return $output
}

function SimpleFileNameFriendlyDate($dateTime)
{
    return $dateTime.ToString("yyyy-MM-dd")
}

function FormatFileNameFriendlyDate($dateTime)
{
        $formatedDate = $dateTime.ToUniversalTime().ToString("u") 
        $formatedDate = $formatedDate.Replace(" ", "_")
        $formatedDate = $formatedDate.Replace(":", "-")
        return $formatedDate
}